#ifndef PiezoBuzzer_h
#define PiezoBuzzer_h


class PiezoBuzzer
{
  public:
    PiezoBuzzer(int pinId);
    void Buzz(int MilliSec);

  private:
   int _pinId;  
};



#endif


