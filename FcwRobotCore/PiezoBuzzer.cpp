#include "PiezoBuzzer.h"
#include "Arduino.h"


//Pin to use for the buzzer
int _pinId;

//Set the pin to use for the buzzer on the construtor
PiezoBuzzer::PiezoBuzzer(int pinId)
{
  //Set the pinId to output.
  pinMode(pinId, OUTPUT);
  _pinId = pinId;
}

//Create a short burst of buzzer sound.
void PiezoBuzzer::Buzz(int MilliSec)
{
  //Check to make sure that the pinId is valid to use.
  if(_pinId > 0)
  {
     digitalWrite(_pinId, HIGH);
     delay(MilliSec);
     digitalWrite(_pinId, LOW);
  }
}


